<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $fillable = [
        'name',
        'category_id'
    ];

    /**
     * 
     * Relationship functions
     */

    public function Category()
    {
        return $this->belongsTo(Category::class);
    }
}
